package dbUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 四则运算工厂类
 * 
 * @author xumz
 * 
 */
public class MathFactory {
    private int radius;// 题目中数值的范围，不包括上限
    private int operatorNum;// 运算符个数
    private int questionNum;// 运算符个数
    private String question;// 单个题目
    private String answer;// 单个答案
    private List<String> questionsList;// 题目集合
    private List<String> answersList;// 答案集合
    private List<String> mathList;// 题目加答案集合
    private List<List<Integer>> number;// 所有计算数的集合
    private List<List<Character>> opeator;// 所有运算符的集合
    private boolean onlyOne=true;//用来判断是否没有调用了productMathAnswerList（）方法

    /**
     * 生产一道计算题
     * 
     * @return String
     */
    String productSingleMath() {
        question = createEasyMathQuestion();
        answer = createMathAnswer();
        return question + "=" + answer;
    }

    /**
     * 生产计算题集合
     * 
     * @return List
     */
    List<String> productMathQuestionList() {
//        onlyOne=false;
        questionsList = new ArrayList<String>();
        for (int i = 0; i < questionNum; i++) {// 设置题数
            questionsList.add(createEasyMathQuestion());
        }
//        onlyOne=true;
        return questionsList;
    }

    /**
     * 生成简单的数学题目，整数运算
     * 
     * @return String
     */
    String createEasyMathQuestion() {
        Random rand = new Random();
        String str1 = "" + rand.nextInt(radius);// 设置计算值范围
        String tempNum = null;// 随机生成的计算数
        String tempOprator = null;// 随机生成的运算符
        // 注意：'/'后面不能跟0
        for (int j = 0; j < operatorNum; j++) {// 这里设置操作符个数
            tempOprator = getOpeator(rand.nextInt(4));// 给你个魔鬼数字，自己看下面方法
            if (tempOprator.equals("/")) {
                tempNum = "" + (rand.nextInt(radius) + 1);// '/'后面不能跟0
            } else {
                tempNum = "" + rand.nextInt(radius);
            }
            str1 += tempOprator + tempNum;
        }
        question = str1;
        return str1;
    }

    /**
     * 获取运算符
     * 
     * @param key
     * @return
     */
    static String getOpeator(int key) {
        String opeator = null;
        switch (key) {
        case 0:
            opeator = "+";
            break;
        case 1:
            opeator = "-";
            break;
        case 2:
            opeator = "*";
            break;
        case 3:
            opeator = "/";
            break;
        default:
            System.out.println("参数输入发生错误");
        }
        return opeator;
    }

    /**
     * 成答案集合
     * 
     * @return
     */
    List<String> productMathAnswerList() {
        onlyOne=false;
        answersList = new ArrayList<String>();
        opeator = new ArrayList<List<Character>>();
        number = new ArrayList<List<Integer>>();
        for (int i = 0; i < questionsList.size(); i++) {
            question = questionsList.get(i);
            answersList.add(createMathAnswer());
        }
        onlyOne=true;
        return answersList;
    }

    // * 思路：将question中的String题目，拿过来，拆分成计算数和运算符，将计算数进行类型转换成可计算的数字
    // 运算符转换成char类型，判断优先级
    /**
     * 生成答案
     * 
     * @return String
     */
    String createMathAnswer() {
        char[] tempStringtoChar = question.toCharArray();// 暂时存放String转换成的字符
        List<Character> opeatorList = new ArrayList<Character>();// 存放运算符
        for (int i = 0; i < tempStringtoChar.length; i++) {
            switch (tempStringtoChar[i]) {
            case '+':
                opeatorList.add('+');
                break;
            case '-':
                opeatorList.add('-');
                break;
            case '*':
                opeatorList.add('*');
                break;
            case '/':
                opeatorList.add('/');
                break;
            default:
                break;
            }
        }
        List<Character> tempOpeatorList = new ArrayList<Character>();// 存放运算符备份
        tempOpeatorList.addAll(opeatorList);
        if (!onlyOne) {
            opeator.add(tempOpeatorList);
        }
        // 计算答案
        // 思路：将运算符两边的计算数进行相应运算，将结果存赋值给第一个计算数，移除第二个计算数，最终只剩下一个计算结果
        String[] tempStringNum = question.split("[+ * / -]");// 暂时存放String类型的计算数字
        List<Integer> numList = new ArrayList<Integer>();// 存放计算数集合
        for (int i = 0; i < tempStringNum.length; i++) {
            numList.add(Integer.parseInt(tempStringNum[i]));
        }
        List<Integer> tempNumList = new ArrayList<Integer>();// 存放计算数集合备份
        tempNumList.addAll(numList);
        if (!onlyOne) {
            number.add(tempNumList);
        }

        // 除法运算，转换成分数形式
        // 思路：
        //1处理连续的乘除，将式子简化，如0+1+2/3/4+5/6*7+8 简化成0+1+2/12+35/6+8
        //2、集中所有'/'后面的计算数divisor，求公倍数，作为所有计算数的分母denominator
        //3、消除 * / 使式子只剩下加减法运算
        // 4、将所得数进行后面的加减法运算
        // 5、最后添上最大公约数为分母，约分。
        int denominator = 1;// 公倍数
        boolean divFlag = false;
        if (opeatorList.contains('/')) {
            divFlag = true;
            // 将除数，化简成，连续除的只有一个除数。例如：1/2/3 =》 1/6
            // 1/2/3*4 => 4/6
            for (int i = 0; i < opeatorList.size(); i++) {
                if (opeatorList.get(i).equals('/')) {
                    for (int j = i + 1; j < opeatorList.size(); j++) {
                        if (opeatorList.get(j).equals('/')) {
                            numList.set(j, numList.get(j) * numList.get(j + 1));
                        } else if (opeatorList.get(j).equals('*')) {
                            numList.set(i, numList.get(j + 1) * numList.get(i));
                        } else {
                            break;
                        }
                        opeatorList.remove(j);
                        numList.remove(j + 1);
                        j--;
                    }
                }
            }
            // System.out.println("第一次化简" + numList);
            // System.out.println("第一次化简" + opeatorList);
            // 求最公倍数
            for (int i = 0; i < opeatorList.size(); i++) {
                if (opeatorList.get(i) == '/') {
                    denominator *= numList.get(i + 1);
                }
            }
            // System.out.println("公倍数：" + denominator);
            // 消除 *运算符
            if (opeatorList.contains('*')) {
                for (int i = 0; i < opeatorList.size(); i++) {
                    if (opeatorList.get(i) == '*') {
                        numList.set(i, (numList.get(i) * numList.get(i + 1)));
                        numList.remove(i + 1);
                        opeatorList.remove(i);
                        i--;
                    }
                }
            }
            // System.out.println("消除*：" + numList);
            // System.out.println("消除*：" + opeatorList);
            // 给非乘除运算的数乘公倍数 例如：1+2/3+4 中的1和4
            for (int i = 0; i < opeatorList.size(); i++) {
                // System.out.println("opeatorList进来：" + opeatorList.get(i));
                boolean condition = opeatorList.get(i).equals('+')
                        || opeatorList.get(i).equals('-');
                // System.out.println(condition);
                if (condition) {
                    if (i == 0) {// 首部特殊情况
                        numList.set(0, numList.get(0) * denominator);
                    } else if (i == opeatorList.size() - 1) {// 尾部2个部特殊情况
                        numList.set(numList.size() - 1,
                                numList.get(numList.size() - 1) * denominator);
                        if (opeatorList.get(i - 1).equals('+')
                                || opeatorList.get(i - 1).equals('-')) {
                            numList.set(i, numList.get(i) * denominator);
                        }
                    } else {
                        if (opeatorList.get(i - 1).equals('+')
                                || opeatorList.get(i - 1).equals('-')) {
                            numList.set(i, numList.get(i) * denominator);
                        }
                    }
                }
            }
            // System.out.println("给非乘除运算倍化" + numList);
            // System.out.println("给非乘除运算倍化" + opeatorList);

            // 消除 / 运算符
            for (int i = 0; i < opeatorList.size(); i++) {
                if (opeatorList.get(i).equals('/')) {
                    numList.set(i,
                            (numList.get(i) * denominator / numList.get(i + 1)));
                    numList.remove(i + 1);
                    opeatorList.remove(i);
                    i--;
                }
            }
            // System.out.println("消除/：" + numList);
            // System.out.println("消除/：" + opeatorList);
        }
        // 消除 * 运算符
        if (!divFlag) {
            if (opeatorList.contains('*')) {
                for (int i = 0; i < opeatorList.size(); i++) {
                    if (opeatorList.get(i) == '*') {
                        numList.set(i, (numList.get(i) * numList.get(i + 1)));
                        numList.remove(i + 1);
                        opeatorList.remove(i);
                        i--;
                    }
                }
                // System.out.println("消除*：" + numList);
                // System.out.println("消除*：" + opeatorList);
            }
        }

        // 最后进行加减法运算
        if (numList.size() != 1) {
            for (int i = 0; i < opeatorList.size(); i++) {
                switch (opeatorList.get(i)) {
                case '+':
                    numList.set(0, (numList.get(0) + numList.get(1)));
                    break;
                case '-':
                    numList.set(0, (numList.get(0) - numList.get(1)));
                    break;
                default:
                    break;
                }
                numList.remove(1);

            }
        }
        // 計算完成
        if (divFlag) {
            answer = reductionFun(denominator, numList.get(0));
            return answer;
        } else {
            answer = "" + numList.get(0);
            return answer;
        }
    }

    /**
     * 约分化简
     * 
     * @param denominator
     * @param numerator
     * @return
     */
    public String reductionFun(int denominator, int numerator) {
        if (denominator == 0) {
            System.out.println("分母不能为0");
            return null;
        }
        if (numerator == 0) {
            return "0";
        }
        // 判断符号
        String flag;
        if ((denominator < 0 && numerator < 0)
                || (denominator > 0 && numerator > 0)) {
            flag = "";
        } else {
            flag = "-";
        }
        denominator = Math.abs(denominator);
        numerator = Math.abs(numerator);
        // 即求出最大公因数
        int smaller = numerator > denominator ? numerator : denominator;
        int maxCommonFactor = 1;
        for (int i = 1; i <= smaller; i++) {
            if (numerator % i == 0 && denominator % i == 0) {
                maxCommonFactor = i;
            }
        }
        // 可化为整数
        if (((numerator / maxCommonFactor) % (denominator / maxCommonFactor)) == 0) {
            int res = (numerator / maxCommonFactor)
                    / (denominator / maxCommonFactor);
            if (res == 0) {
                return "" + res;
            }
            return flag + res;
        }
        return flag + numerator / maxCommonFactor + "/" + denominator
                / maxCommonFactor;
    }

    /**
     * 返回重复题目集合信息
     * 
     * @return
     */
    public List<String> getRepeatInfo() {
        List<String> repeatInfo = new ArrayList<String>();
        productMathAnswerList();
        // 判断答案是否一样
        for (int i = 0; i < answersList.size() - 1; i++) {
            for (int j = i + 1; j < answersList.size(); j++) {
                if (answersList.get(i).equals(answersList.get(j))) {
                    // 判断两个计算数是否含有完全一样的所有数字
                    if (number.get(i).containsAll(number.get(j))&&number.get(j).containsAll(number.get(i))) {
                        // 判断两个运算符的每一个位置是否一致
                        boolean flag = true;
                        for (int k = 0; k < opeator.get(i).size(); k++) {
                            if (!opeator.get(i).get(k).equals(opeator.get(j).get(k))) {
                                flag = false;
                                break;
                            }
                        }
                        if (flag) {
                            repeatInfo.add("第" + (i+1) + "道题与第" + (j+1) + "道题重复,题型:"
                                    + questionsList.get(i)+"="+questionsList.get(j));
                        }

                    }
                }
            }
        }
        return repeatInfo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public List<String> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<String> questionsList) {
        this.questionsList = questionsList;
    }

    public List<String> getAnswersList() {
        return answersList;
    }

    public void setAnswersList(List<String> answersList) {
        this.answersList = answersList;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getOperatorNum() {
        return operatorNum;
    }

    public void setOperatorNum(int operatorNum) {
        this.operatorNum = operatorNum;
    }

    public List<String> getMathList() {
        return mathList;
    }

    public void setMathList(List<String> mathList) {
        this.mathList = mathList;
    }

    public int getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(int questionNum) {
        this.questionNum = questionNum;
    }

    public List<List<Integer>> getNumber() {
        return number;
    }

    public void setNumber(List<List<Integer>> number) {
        this.number = number;
    }

    public List<List<Character>> getOpeator() {
        return opeator;
    }

    public void setOpeator(List<List<Character>> opeator) {
        this.opeator = opeator;
    }

    public String toString() {
        return "MathFactory [radius=" + radius + ", operatorNum=" + operatorNum
                + "]";
    }

}