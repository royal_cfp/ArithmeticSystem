package com.jmu.factory;

import java.util.ArrayList;
import java.util.List;

import com.jmu.bean.Bean;

public class AnswerFactroy {
	/**
	 * 生成答案
	 * @param question
	 * @return
	 */
	public static String createMathAnswer(String question) {
		char[] tempStringtoChar = question.toCharArray();// 暂时存放String转换成的字符
		List<Character> opeatorList = new ArrayList<Character>();// 存放运算符
		for (int i = 0; i < tempStringtoChar.length; i++) {
			switch (tempStringtoChar[i]) {
			case '+':
				opeatorList.add('+');
				break;
			case '-':
				opeatorList.add('-');
				break;
			case '*':
				opeatorList.add('*');
				break;
			case '/':
				opeatorList.add('/');
				break;
			case '^':
				opeatorList.add('^');
				break;
			case '÷':
				opeatorList.add('/');
				break;
			}
		}
//		List<Character> tempOpeatorList = new ArrayList<Character>();// 存放运算符备份
//		tempOpeatorList.addAll(opeatorList);
		// 计算答案
		// 思路：将运算符两边的计算数进行相应运算，将结果存赋值给第一个计算数，移除第二个计算数，最终只剩下一个计算结果
		String[] tempStringNum = question.split("[+ * / ÷ ^ -]");// 暂时存放String类型的计算数字
		List<Integer> numList = new ArrayList<Integer>();// 存放计算数集合
		for (int i = 0; i < tempStringNum.length; i++) {
			numList.add(Integer.parseInt(tempStringNum[i]));
		}
//		List<Integer> tempNumList = new ArrayList<Integer>();// 存放计算数集合备份
//		tempNumList.addAll(numList);

		// 除法运算，转换成分数形式
		// 思路：
		// 0、处理真分数，将其转化成除法，1^1/3 => 4/3
		// 1、处理连续的乘除，将式子简化，如0+1+2/3/4+5/6*7+8 简化成0+1+2/12+35/6+8
		// 2、集中所有'/'后面的计算数divisor，求公倍数，作为所有计算数的分母denominator
		// 3、消除 * / 使式子只剩下加减法运算
		// 4、将所得数进行后面的加减法运算
		// 5、最后添上最大公约数为分母，约分。
		for (int i = 0; i < opeatorList.size(); i++) {
			if (opeatorList.get(i).equals('^')) {
				numList.set(i+1, numList.get(i)*numList.get(i+2)+numList.get(i+1));
				numList.remove(i);
				opeatorList.remove(i);
			}
		}
		int denominator = 1;// 公倍数
		boolean divFlag = false;
		if (opeatorList.contains('/')) {
			divFlag = true;
			// 将除数，化简成，连续除的只有一个除数。例如：1/2/3 =》 1/6
			// 1/2/3*4 => 4/6
			for (int i = 0; i < opeatorList.size(); i++) {
				if (opeatorList.get(i).equals('/')) {
					for (int j = i + 1; j < opeatorList.size(); j++) {
						if (opeatorList.get(j).equals('/')) {
							numList.set(j, numList.get(j) * numList.get(j + 1));
						} else if (opeatorList.get(j).equals('*')) {
							numList.set(i, numList.get(j + 1) * numList.get(i));
						} else {
							break;
						}
						opeatorList.remove(j);
						numList.remove(j + 1);
						j--;
					}
				}
			}
			// System.out.println("第一次化简" + numList);
			// System.out.println("第一次化简" + opeatorList);
			// 求最公倍数
			for (int i = 0; i < opeatorList.size(); i++) {
				if (opeatorList.get(i) == '/') {
					denominator *= numList.get(i + 1);
				}
			}
			// System.out.println("公倍数：" + denominator);
			// 消除 *运算符
			if (opeatorList.contains('*')) {
				for (int i = 0; i < opeatorList.size(); i++) {
					if (opeatorList.get(i) == '*') {
						numList.set(i, (numList.get(i) * numList.get(i + 1)));
						numList.remove(i + 1);
						opeatorList.remove(i);
						i--;
					}
				}
			}
			// System.out.println("消除*：" + numList);
			// System.out.println("消除*：" + opeatorList);
			// 给非乘除运算的数乘公倍数 例如：1+2/3+4 中的1和4
			for (int i = 0; i < opeatorList.size(); i++) {
				// System.out.println("opeatorList进来：" + opeatorList.get(i));
				boolean condition = opeatorList.get(i).equals('+')
						|| opeatorList.get(i).equals('-');
				// System.out.println(condition);
				if (condition) {
					if (i == 0) {// 首部特殊情况
						numList.set(0, numList.get(0) * denominator);
					} else if (i == opeatorList.size() - 1) {// 尾部2个部特殊情况
						numList.set(numList.size() - 1,
								numList.get(numList.size() - 1) * denominator);
						if (opeatorList.get(i - 1).equals('+')
								|| opeatorList.get(i - 1).equals('-')) {
							numList.set(i, numList.get(i) * denominator);
						}
					} else {
						if (opeatorList.get(i - 1).equals('+')
								|| opeatorList.get(i - 1).equals('-')) {
							numList.set(i, numList.get(i) * denominator);
						}
					}
				}
			}
			// System.out.println("给非乘除运算倍化" + numList);
			// System.out.println("给非乘除运算倍化" + opeatorList);

			// 消除 / 运算符
			for (int i = 0; i < opeatorList.size(); i++) {
				if (opeatorList.get(i).equals('/')) {
					numList.set(i,
							(numList.get(i) * denominator / numList.get(i + 1)));
					numList.remove(i + 1);
					opeatorList.remove(i);
					i--;
				}
			}
			// System.out.println("消除/：" + numList);
			// System.out.println("消除/：" + opeatorList);
		}
		// 消除 * 运算符
		if (!divFlag) {
			if (opeatorList.contains('*')) {
				for (int i = 0; i < opeatorList.size(); i++) {
					if (opeatorList.get(i) == '*') {
						numList.set(i, (numList.get(i) * numList.get(i + 1)));
						numList.remove(i + 1);
						opeatorList.remove(i);
						i--;
					}
				}
				// System.out.println("消除*：" + numList);
				// System.out.println("消除*：" + opeatorList);
			}
		}

		// 最后进行加减法运算
		if (numList.size() != 1) {
			for (int i = 0; i < opeatorList.size(); i++) {
				switch (opeatorList.get(i)) {
				case '+':
					numList.set(0, (numList.get(0) + numList.get(1)));
					break;
				case '-':
					numList.set(0, (numList.get(0) - numList.get(1)));
					break;
				default:
					break;
				}
				numList.remove(1);

			}
		}
		// 計算完成
		Bean bean;
		if (divFlag) {
			bean = new Bean( numList.get(0),denominator);
			return bean.toString();
		} else {
			bean = new Bean( numList.get(0),1);
			return bean.toString();
		}
	}
	
	/**
	 *  生成答案集合
	 * @param mathList
	 * @return ArrayList<String> answersList
	 */
    public static List<String> productMathAnswerList(List<String> mathList) {
    	List<String> answersList=new ArrayList<String>();
        for (int i = 0; i < mathList.size(); i++) {
            answersList.add(createMathAnswer(mathList.get(i)));
        }
        return answersList;
    }
}
