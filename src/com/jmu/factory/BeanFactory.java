package com.jmu.factory;

import java.util.Random;

import com.jmu.bean.Bean;

public class BeanFactory {
	public static Random rand = new Random();
	/**
	 * 生成运算数
	 * @param int radius
	 * @return Bean
	 */
	public static Bean createRadomBeanNum(int radius){
		int numerator = rand.nextInt(radius);
		int denominator = rand.nextInt(radius)+1;
		Bean bean=new Bean(numerator, denominator);
		return bean;
	}
	/**
	 * 生成非0运算数
	 * @param int radius
	 * @return Bean
	 */
	public static Bean createRadomBeanNumExcpetZero(int radius){
		int numerator = rand.nextInt(radius)+1;
		int denominator = rand.nextInt(radius)+1;
		Bean bean=new Bean(numerator, denominator);
		return bean;
	}
	/**
	 *  生成运算符
	 * @return bean
	 */
	public static Bean createRadomBeanOperator(){
		int key = rand.nextInt(4);
		String operator="";
		switch (key) {
        case 0:
        	operator = "+";
            break;
        case 1:
        	operator = "-";
            break;
        case 2:
        	operator = "*";
            break;
        case 3:
        	operator = "÷";
            break;
        }
		Bean bean = new Bean(operator);
		return bean;
	}
	
}
