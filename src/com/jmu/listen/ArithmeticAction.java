package com.jmu.listen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import com.jmu.factory.AnswerFactroy;
import com.jmu.factory.MathFactory;

public class ArithmeticAction {
	int count=1;	
	public void CreateMath(int radius,int number) {
		
		int operatorNum=3 ;		
		for (int i = 0; i < number; i++) {
			String mathStr=MathFactory.createMathProblem(radius, operatorNum);
			String answerStr=AnswerFactroy.createMathAnswer(mathStr);
			try {
				WriteStringFile("C:\\Arithmetic\\math.txt", mathStr);
				WriteStringFile("C:\\Arithmetic\\answer.txt", answerStr);
				count++;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}
	public void WriteGradeFile(String f,String[] str ,String time) throws IOException{
		String[]  grade=new String[4];
        grade[0]="TotalCorrect: ";
        grade[1]="TotalWrong: ";
        grade[2]="TotalRepeat: ";
        grade[3]="ThisTestUsedTime: "+time;
        int c=0,w=0,r=0;
        File file=new File("C:\\Arithmetic\\grade.txt");
        if(file.exists()==true){
        	System.out.println(c+"fff");
        FileReader fr=new FileReader(file);
    	BufferedReader b = new BufferedReader(fr);
    	 String[] rub=new String[2];
    	 rub= b.readLine().split("\\:");
    	 System.out.println(rub[1]+"fff");
    	c=Integer.parseInt(rub[1].trim());
    	System.out.println(c+"fff");
    	rub= b.readLine().split("\\:");
    	w=Integer.parseInt(rub[1].trim());
    	rub= b.readLine().split("\\:");
    	r=Integer.parseInt(rub[1].trim());
    	b.close();
    	fr.close();
    	System.out.println(c+"fff");
        }
    	String[] ans=new String[str.length];
        try {
        	BufferedReader br = new BufferedReader(new  InputStreamReader(new FileInputStream(new File(f)),"UTF-8"));
        	String lineTxt = null;
        	int j=1;
        	while ((lineTxt = br.readLine()) != null) {
        		 try {
        			 String[] rub=new String[2];  
        			 rub= lineTxt.split("\\)");
        			 ans[j-1]=rub[1].trim();
        			 if(ans[j-1].equals(str[j-1])){
        				 c++;
        			 }
        			 else{
        				 w++;
        			 }
        		 }
        		 catch (Exception e) {
        				// TODO Auto-generated catch block
        				//System.out.println(j+"  答案为空");
        				w++;
        		  }
        		 j++;
        	}
        	br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        grade[0]=grade[0]+c;
        grade[1]=grade[1]+w;
        grade[2]=grade[2]+r;
        
		if(file.exists()==true){
		file.delete();
		}
	    file.createNewFile();
	    FileWriter fileWritter = new FileWriter(file,true);
	    BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
	    for(int i=0;i<grade.length;i++){
	         bufferWritter.write(i+1+".  "+grade[i]);
	         bufferWritter.newLine();
	    }
	    bufferWritter.close();
	    fileWritter.close();
	}
	public void WriteStringFile(String f,String str) throws IOException{
		System.out.println(count);
		File file=new File(f);
		if(count==1||file.exists()==false){
		if(file.exists()==true){
		file.delete();
		}
	    file.createNewFile();
		}
		System.out.println(file.getAbsolutePath());
		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bw = new BufferedWriter(fw);
//		bw.append("在已有的基础上添加字符串");
		if(f.equals("C:\\Arithmetic\\math.txt")){
			bw.write("("+count+") "+str+"=\r\n");// 往已有的文件上添加字符串
		}
		else{
		bw.write("("+count+") "+str+"\r\n");// 往已有的文件上添加字符串
		}
		bw.close();
		fw.close();
	}
	public int getTextLines(String path) throws IOException {
		   FileReader fr = new FileReader(path);   //这里定义一个字符流的输入流的节点流，用于读取文件（一个字符一个字符的读取）
		   BufferedReader br = new BufferedReader(fr);  // 在定义好的流基础上套接一个处理流，用于更加效率的读取文件（一行一行的读取）
		   int x = 0;   // 用于统计行数，从0开始
		   while(br.readLine() != null) { //  readLine()方法是按行读的，返回值是这行的内容
		   	x++;   // 每读一行，则变量x累加1
		   }
		   return x;  //返回总的行数
	}	
}
