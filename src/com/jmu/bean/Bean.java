package com.jmu.bean;

public class Bean {
	private String operator;// 运算符
	private boolean opFlag = false;// 用了表示是否为运算符 true->运算符 ，false->运算数
	private int numerator;// 分子
	private int denominator = 1;// 分母,分母不能为0;
	private boolean imfraFlage = false;// 是否是假分数 improper fraction
	private int head;// 假分数的头
	private int minNumerator;// 分子大于分母时的余数

	public Bean() {
		// TODO Auto-generated constructor stub
	}

	public Bean(String operator) {
		super();
		this.operator = operator;
		opFlag=true;
	}

	public Bean(int numerator, int denominator) {
		super();
		this.numerator = numerator;
		this.denominator = denominator;
		opFlag=false;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public boolean isOpFlag() {
		return opFlag;
	}

	public void setOpFlag(boolean opFlag) {
		this.opFlag = opFlag;
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		if (denominator == 0) {
			System.out.println("分母不能为");
			return;
		}
		this.denominator = denominator;
	}

	public boolean isImfraFlage() {
		return imfraFlage;
	}

	public void setImfraFlage(boolean imfraFlage) {
		this.imfraFlage = imfraFlage;
	}

	public int getHead() {
		return head;
	}

	public void setHead(int head) {
		this.head = head;
	}

	public int getMinNumerator() {
		return minNumerator;
	}

	public void setMinNumerator(int minNumerator) {
		this.minNumerator = minNumerator;
	}

	// toString 显示方法
	@Override
	public String toString() {
		// 运算数输出，运算符输出
		if (!opFlag) {
			// 整除整数输出
			if (numerator % denominator == 0) {
				return "" + numerator / denominator;
			}
			simpleNum();
			checkBean();
			// 假分数，化为真分数后输出
			if (imfraFlage) {
				String minus = "";
				if (minNumerator < 0) {
					minus = "-";
					minNumerator = Math.abs(minNumerator);
				}
				return minus + head + "^" + minNumerator + "/" + denominator;
			} else {
				return "" + numerator + "/" + denominator;
			}

		} else {
			return operator;
		}
	}
	// 约分
	public void simpleNum() {
		// 即求出最大公因数
		int smaller = numerator > denominator ? numerator : denominator;
		int maxCommonFactor = 1;
		for (int i = 1; i <= smaller; i++) {
			if (numerator % i == 0 && denominator % i == 0) {
				maxCommonFactor = i;
			}
		}
		numerator=numerator/maxCommonFactor;
		denominator=denominator/maxCommonFactor;
	}

	// 判断是否为假分数，如果是的话，对一些参数进行赋值
	public void checkBean() {
		if (denominator == 0) {
			System.out.println("参数错误，分母不能为0");
			return;
		}
		if (numerator == 0) {
			return;
		}
		if (Math.abs(numerator) > denominator) {
			imfraFlage = true;
			head = Math.abs(numerator) / denominator;
			minNumerator = numerator % denominator;
		} else {
			imfraFlage = false;
		}
	}

}
