package com.jmu;

import java.io.IOException;

import com.jmu.listen.ArithmeticAction;
import com.opensymphony.xwork2.ActionSupport;

public class MathAction extends ActionSupport {

	/**
	 * @return
	 */
	public int radius;
	public int number;
    public String[] answers;
    public String lazy;
   
	public String getLazy() {
		return lazy;
	}

	public void setLazy(String lazy) {
		this.lazy = lazy;
	}


	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String[] getAnswers() {
		return answers;
	}

	public void setAnswers(String[] answers) {
		this.answers = answers;
	}

	public String createMath() {
		// TODO Auto-generated method stub
		System.out.print(number+"::"+radius);
		ArithmeticAction m=new ArithmeticAction();
		m.CreateMath(radius, number);
		return "create";
	}
	public String submitAnswers(){
		ArithmeticAction m=new ArithmeticAction();
		String f="C:\\Arithmetic\\answer.txt";
		System.out.print(answers.length+"::"+lazy);
		try {
			m.WriteGradeFile(f, answers,lazy);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "reply";
	}
}