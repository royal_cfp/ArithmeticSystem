<%@ page contentType="text/html; charset=GBK" language="java" import="java.sql.*" errorPage="" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.File"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>四则运算在线答题系统</title>
 <style type="text/css">

.face {
  text-align: right;
  padding-right: 60px;
}

.face p {
  font-size: 30px;
  color:green;
}
.txt{
margin-left: 200px;
color: white;
}
 </style>
  </head>
  
  <body>

    <%
    String content="C:\\Arithmetic\\math.txt";
    //此处写你的txt文件的绝对路径
    FileReader fr = new FileReader(content);
    BufferedReader br = new BufferedReader(fr);
    StringBuffer txt = new StringBuffer();
    String temp = null;
    int i=0;
    while((temp = br.readLine()) != null) {
        txt.append(temp+"<input name='answers'></input><br><br>");
        i++;
    }
    br.close();
    fr.close();
%>
<form action="MathAction!submitAnswers.action"  method="post" namespace="/">
<div class="face" >
   <p id="lazy"  >00:00:00</p> 
   <input type="hidden" id="timer" name="lazy"></input> 
</div>
<script>
var defaults = {}
  , one_second = 1000
  , one_minute = one_second * 60
  , one_hour = one_minute * 60
  , one_day = one_hour * 24
  , startDate = new Date()
  , face = document.getElementById('lazy');

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
var requestAnimationFrame = (function() {
  return window.requestAnimationFrame       || 
         window.webkitRequestAnimationFrame || 
         window.mozRequestAnimationFrame    || 
         window.oRequestAnimationFrame      || 
         window.msRequestAnimationFrame     || 
         function( callback ){
           window.setTimeout(callback, 1000 / 60);
         };
}());

tick();

function tick() {

  var now = new Date()
    , elapsed = now - startDate
    , parts = [];

  parts[0] = '' + Math.floor( elapsed / one_hour );
  parts[1] = '' + Math.floor( (elapsed % one_hour) / one_minute );
  parts[2] = '' + Math.floor( ( (elapsed % one_hour) % one_minute ) / one_second );

  parts[0] = (parts[0].length == 1) ? '0' + parts[0] : parts[0];
  parts[1] = (parts[1].length == 1) ? '0' + parts[1] : parts[1];
  parts[2] = (parts[2].length == 1) ? '0' + parts[2] : parts[2];

  face.innerText = parts.join(':');
  requestAnimationFrame(tick);
   document.getElementById('timer').value=face.innerText;
}
</script>
<br>
<div class="txt">
<%=txt %>
<br>
<input type="submit" align="middle"  data-localize="index.submitAnswer"></input>
</div>
</form>
<br>
<br>
<br>
  </body>
</html>
