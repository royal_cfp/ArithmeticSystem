<%@ page contentType="text/html; charset=GBK" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>四则运算在线答题系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/index.css" />
    <script type="text/javascript" src="js/jquery-3.1.1.js "></script>
    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/jquery.locale.js"></script>
    <script type="text/javascript" src="js/language-coockies.js"></script>
  </head>
  
	<body >
		 <div class="head"><h1 data-localize=”index.title”>
		 <a data-localize="index.title" href=""></a> 
		 </h1>

		 </div>
		 <div class="select" align="right">
		 <select id="ddlSomoveLanguage" onchange="chgLang();" >  
		 		<option data-localize="index.select"></option>  
               <option value="zh">中文简体</option>  
               <option value="en">ENGLISH</option>  
               <option value="zh_cn">中文繁体</option>
           </select> 
           </div>
		 <main class="title">
			<div class="title_left">
			<div class="title_01">
			<a class='a1' data-localize="index.index" href=""></a>
			<a class='a1' data-localize="index.replyOnline" href="index.jsp?filename=replyonline.jsp"></a>
			<a class='a1' data-localize="index.help" href=""></a>
			</div>							
			</div> 
			<div class="title_02">
			<%if(session.getAttribute("username")==null) {%>
				<a class='a1' data-localize="index.login" href=""></a>
				<a class='a1' data-localize="index.register" href=""></a>					
				<%}
				else{
				String user_name=session.getAttribute("username").toString();
				 %>
				 <a class='a1'><%=user_name %>,</a>	
				 <a class='a1' data-localize="index.loginout" href=""></a>	
				 <%} %>
			</div>
		</main>
		<% 
		    String filename="";
		    if(request.getParameter("filename")==null){
		    filename="replyonline.jsp";
		    }
		    else{
		    filename=request.getParameter("filename").toString();
		    }
		 %>
		<jsp:include  page='<%=filename %>' flush="false"></jsp:include>
		<script type="text/javascript" src="js/index.js"></script>
	</body>
</html>
